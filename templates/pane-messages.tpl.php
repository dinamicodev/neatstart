<?php
/**
 * @file
 *
 * Theme implementation to display the messages area, which is normally
 * included roughly in the content area of a page.
 *
 * This utilizes the following variables thata re normally found in
 * page.tpl.php:
 * - $tabs
 * - $messages
 * - $help
 *
 * Additional items can be added via theme_preprocess_pane_messages(). See
 * template_preprocess_pane_messages() for examples.
 */

$tabs = render($tabs);
$action_links = render($action_links)
?>
<?php if (!empty($tabs)): ?>
  <nav class="tabs">
    <?php print $tabs; ?>
  </nav>
<?php endif; ?>
<?php if ($action_links): ?>
  <ul class="action-links">
    <?php print $action_links; ?>
  </ul>
<?php endif; ?>
<?php print $messages; ?>
<?php print $help; ?>
