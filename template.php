<?php
/*
 * @file
 */

require_once('inc/elements.inc');
require_once('inc/form.inc');
require_once('inc/menu.inc');
require_once('inc/theme.inc');
require_once('inc/blockify.inc');

/**
 * Implements hook_css_alter()
 */
function neatstart_css_alter(&$css) {
  // Remove defaults.css file.
  //dsm(drupal_get_path('module', 'system') . '/system.menus.css');
  #unset($css[drupal_get_path('module', 'system') . '/system.menus.css']);
}

/**
 * Implements hook_js_alter()
 */
function neatstart_js_alter(&$js) {
  //
}

/**
 * Implements hook_html_head_alter().
 */
function neatstart_html_head_alter(&$head_elements) {
  // HTML5 charset declaration.
  /*$head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8',
  );*/
  
  // Force IE to use Chrome Frame if installed.
  $head_elements['chrome_frame'] = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'content' => 'ie=edge, chrome=1',
      'http-equiv' => 'x-ua-compatible',
    ),
  );
  
  // Remove image toolbar in IE.
  $head_elements['ie_image_toolbar'] = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'ImageToolbar',
      'content' => 'false',
    ),
  );
}

/**
 * Implements hook_preprocess_block()
 */
function neatstart_preprocess_block(&$variables) { #dpm($variables);
  $block = $variables['block'];
  
  // Panels pane
  if (isset($variables['pane'])) {
    $variables['pane'] = $variables['elements']['#pane'];
  }
  if (isset($block->css_class)) {
    $variables['classes_array'][] = $block->css_class;
  }
  
  #if (isset($block->css_id)) {
  #}
}

/**
 * Implements template_preprocess_field().
 */
function neatstart_preprocess_field(&$variables) { #kpr($variables); exit;
  
  if (!in_array('field', $variables['classes_array'])) {
    array_unshift($variables['classes_array'], 'field');
  }
  
  
  #if ($variables['element']['#field_name'] == 'title') { #dpm($variables);
  #  $variables['theme_hook_suggestions'][] = 'field__no_wrapper';
  #}
  
  #if ($variables['element']['#field_type'] == 'ds' 
  #  && ($variables['theme_hook_suggestions'][0] == 'f' || $variables['theme_hook_suggestions'][0] == 'field__fences_div')) { 
  #  array_unshift($variables['theme_hook_suggestions'], 'field__fences_p'); #kpr($variables);
  #}
  
  
  #$variables['title_attributes_array']['class'][] = 'field-label';
  
  // Edit classes for taxonomy term reference fields.
  #if ($variables['field_type_css'] == 'taxonomy-term-reference') {
  #  $variables['content_attributes_array']['class'][] = 'comma-separated';
  #}
  
}

/**
 * Implements template_preprocess_html().
 *
 * Adds additional classes
 */
function neatstart_preprocess_html(&$variables) {
  //global $language;
  
  // Add language body class.
  #if (function_exists('locale')) {
  #  $variables['classes_array'][] = 'lang-' . $variables['language']->language;
  #}
  
  // Add Foundation scripts
  $theme_path = drupal_get_path('theme', 'neatstart');
  $neatstart_scripts_list = theme_get_setting('neatstart_scripts'); #dpm($neatstart_scripts_list);
  
  if (!empty($neatstart_scripts_list) && is_array($neatstart_scripts_list)) {
    foreach ($neatstart_scripts_list as $script) {
      drupal_add_js($theme_path . '/lib/foundation4/js/foundation/'. $script);
    }
  }
}

/**
 * Implements template_preprocess_page
 *
 * Add convenience variables and template suggestions
 */
function neatstart_preprocess_page(&$variables) { #dpm($variables);
  
  // Add page--node_type.tpl.php suggestions
  if (!empty($variables['node'])) {
    $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
  }
  
  // Top bar.
  if ($variables['top_bar'] = theme_get_setting('neatstart_top_bar_enable')) {
    $top_bar_classes = array();

    if (theme_get_setting('neatstart_top_bar_grid')) {
      $top_bar_classes[] = 'contain-to-grid';
    }

    if (theme_get_setting('neatstart_top_bar_sticky')) {
      $top_bar_classes[] = 'sticky';
    }

    if ($variables['top_bar'] == 2) {
      $top_bar_classes[] = 'show-for-small';
    }
    
    $variables['top_bar_classes'] = implode(' ', $top_bar_classes);
    $variables['top_bar_menu_text'] = theme_get_setting('neatstart_top_bar_menu_text');
  }

  // Alternative header.
  // This is what will show up if the top bar is disabled or enabled only for mobile.
  if ($variables['alt_header'] = ($variables['top_bar'] != 1)) {
    // Hide alt header on mobile if using top bar in mobile.
    $variables['alt_header_classes'] = $variables['top_bar'] == 2 ? ' hide-for-small' : '';
  }
}


/**
 * Implements template_preprocess_node
 *
 * Add template suggestions and classes
 */
function neatstart_preprocess_node(&$variables) { #dpm($variables);
  // Add node--node_type--view_mode.tpl.php suggestions
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'];

  // Add node--view_mode.tpl.php suggestions
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['view_mode'];
}


/**
 * Override or insert variables into the panels-pane templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function neatstart_preprocess_panels_pane(&$variables, $hook) {
  $pane = $variables['pane'];
  #dpm($variables);

  if (isset($pane->configuration['display'])) {
    $variables['classes_array'][] = 'pane-display-id-' . $pane->configuration['display'];
  }

  // Use no pane wrapper for common page elements.
  /*switch ($variables['pane']->subtype) {
    case 'page_content':
    case 'pane_header':
    case 'pane_messages':
    case 'pane_navigation':
      // Allow a pane-specific template to override Zen's suggestion.
      array_unshift($variables['theme_hook_suggestions'], 'panels_pane__no_wrapper');
      break;
  }*/
  // Add component-style class name to pane title.
  //$variables['title_attributes_array']['class'][] = 'pane__title';
}


/**
 * Implements template_preprocess_panels_pane().
 *
 */
/*function neatstart_preprocess_panels_pane(&$variables) {
}*/

/**
* Implements template_preprocess_views_views_fields().
*/
/* Delete me to enable
function THEMENAME_preprocess_views_view_fields(&$variables) {
 if ($variables['view']->name == 'nodequeue_1') {

   // Check if we have both an image and a summary
   if (isset($variables['fields']['field_image'])) {

     // If a combined field has been created, unset it and just show image
     if (isset($variables['fields']['nothing'])) {
       unset($variables['fields']['nothing']);
     }

   } elseif (isset($variables['fields']['title'])) {
     unset ($variables['fields']['title']);
   }

   // Always unset the separate summary if set
   if (isset($variables['fields']['field_summary'])) {
     unset($variables['fields']['field_summary']);
   }
 }
}


/**
 * Implements template_preprocess_views_view().
 */
/*function neatstart_preprocess_views_view(&$variables) {
}*/

