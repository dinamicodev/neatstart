<?php

function neatstart_menu_link__menu_block($variables) { 
  $element = $variables['element'];
  $sub_menu = '';
  
  // Sanitize title
  $element['#title'] = check_plain($element['#title']);
  $element['#localized_options']['html'] = TRUE;
  
  if ($element['#below']) {    
    $element['#attributes']['class'][] = 'has-dropdown';
    unset($element['#below']['#theme_wrappers']);
    $sub_menu = '<ul class="dropdown">' . drupal_render($element['#below']) . '</ul>';
  }
  
  $output = l('<span>' . $element['#title'] . '</span>', $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements theme_links() targeting the main menu specifically.
 * Formats links for Top Bar http://foundation.zurb.com/docs/components/top-bar.html
 */
function neatstart_links__system_main_menu($variables) {    
  $menu_links = menu_tree_output(menu_tree_all_data(variable_get('menu_main_links_source', 'main-menu'))); #dpm($menu_links);
  $output = _neatstart_menu_tree($menu_links);
  return '<ul class="left">' . $output . '</ul>';
}

/**
 * Implements theme_links() targeting the main menu specifically.
 * Formats links for Top Bar http://foundation.zurb.com/docs/components/top-bar.html
 */
function neatstaccccrt_links__system_main_menu($variables) {    
  $menu_links = menu_tree_output(menu_tree_all_data(variable_get('menu_main_links_source', 'main-menu'))); #dpm($menu_links);
  $output = _neatstart_menu_tree($menu_links);
  return '<ul class="left">' . $output . '</ul>';
}

/**
 * Helper function to output menus with Foundation-friendly markup.
 *
 * @param array
 *   An array of menu links.
 *
 * @return string
 *   A rendered list of links, without a <ul> or <ol> wrapper.
 *
 * @see neatstart_links__system_main_menu()
 * @see neatstart_links__system_secondary_menu()
 */
function _neatstart_menu_tree($menu_links) { #dpm($menu_links);
  // Initialize some variables to prevent errors
  $output = '';
  $sub_menu = '';
  $small_link = '';
  
  foreach ($menu_links as $key => $link) { #dpm($link);
    // Add special class needed for Foundation dropdown menu to work
    $small_link = $link; //duplicate version that won't get the dropdown class, save for later
    
    if (!empty($link['#below'])) {
      $link['#attributes']['class'][] = 'has-dropdown';
    }
    
    // Render top level and make sure we have an actual link
    if (!empty($link['#href'])) {
      $output .= '<li' . drupal_attributes($link['#attributes']) . '>' . l($link['#title'], $link['#href']);
      
      // Uncomment if we don't want to repeat the links under the dropdown for large-screen
      #$small_link['#attributes']['class'][] = 'show-for-small';   
      #$sub_menu = '<li' . drupal_attributes($small_link['#attributes']) . '>' . l($link['#title'], $link['#href']);
      
      // Get sub navigation links if they exist
      foreach ($link['#below'] as $sub_link) {
        if (!empty($sub_link['#href'])) {
          $sub_menu .= '<li>' . l($sub_link['#title'], $sub_link['#href']) . '</li>';
        }
      }
      $output .= !empty($link['#below']) ? '<ul class="dropdown">' . $sub_menu . '</ul>' : '';

      // Reset dropdown to prevent duplicates
      unset($sub_menu);
      unset($small_link);
      $small_link = '';
      $sub_menu = '';

      $output .=  '</li>';
    }
  }

  return $output;
}





/**
 * Generate the HTML output for a menu link and submenu.
 *
 * @param $variables
 *  An associative array containing:
 *   - element: Structured array data for a menu link.
 *
 * @return
 *  A themed HTML string.
 *
 * @ingroup themeable
 *
 * Borrowed from theme Basic (template.php)
 */
/*function neatstart_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  // Adding a class depending on the TITLE of the link (not constant)
  $element['#attributes']['class'][] = _neatstart_id_safe($element['#title']);
  // Adding a class depending on the ID of the link (constant)
  $element['#attributes']['class'][] = 'mid-' . $element['#original_link']['mlid'];
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}*/


/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 * - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 * - Replaces any character except A-Z, numbers, and underscores with dashes.
 * - Converts entire string to lowercase.
 *
 * @param $string
 *  The string
 * @return
 *  The converted string
 *
 * Borrowed from theme Basic (template.php)
 */
function _neatstart_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id'. $string;
  }
  return $string;
}
